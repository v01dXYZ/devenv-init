# Devenv: Standardized Work Dev Env
## Emacs Configuration 

| Packages             | Description                                                         |
|----------------------|---------------------------------------------------------------------|
| bind-key             | A simple way to manage personal keybindings                         |
| use-package          |                                                                     |
| async                |                                                                     |
| s                    |                                                                     |
| auto-package-update  |                                                                     |
| ivy                  | Incremental Vertical completion                                     |
| swiper               | Isearch with an overview                                            |
| counsel              | various completion using ivy                                        |
| counsel-etags        |                                                                     |
| window-numbering     |                                                                     |
| wgrep                | writable grep buffer and apply the changes to file                  |
| edit-server          | server that responds to edit requests from Chrome                   |
| origami              | Flexible text folding                                               |
| rainbow-delimiters   |                                                                     |
| beacon               | Highlight the cursor whenever the window scrolls                    |
| which-key            | Display available keybindings in popup                              |
| avy                  | Jump to arbitraty positions in visible text and select text quickly |
| zzz-to-char          | Fancy version of `zap-to-char` command                              |
| realgud              |                                                                     |
| elpy                 |                                                                     |
| yapfify              | format python buffers using YAPF                                    |
| clang-format         |                                                                     |
| modern-cpp-font-lock | Font-locking for "Modern C++"                                       |
| cc-mode              |                                                                     |
| google-c-style       |                                                                     |
| whitespace           |                                                                     |
| company              |                                                                     |
| company-jedi         |                                                                     |
| flycheck             |                                                                     |
| flycheck-pyflakes    |                                                                     |
| string-inflection    |                                                                     |
| multiple-cursors     |                                                                     |
| writegood-mode       | Polish up poor writing on the fly                                   |
| vlf                  | View Large Files                                                    |
| web-mode             |                                                                     |
| autopair             |                                                                     |
| hungry-delete        | Hungry delete for all modes                                         |
| cuda-mode            |                                                                     |
| flyspell             |                                                                     |
| flyspell-correct-ivy |                                                                     |
| magit                |                                                                     |
| git-gutter           | Highlights changes and the type of them in buffers                  |
| cmake-mode           |                                                                     |
| cmake-font-lock      |                                                                     |
| yaml-mode            |                                                                     |
| json-mode            |                                                                     |
| dockerfile-mode      |                                                                     |
| yasnippet            |                                                                     |
| yasnippet-snippets   |                                                                     |
| company-yasnippet    |                                                                     |
| asm-mode             |                                                                     |
| markdown-mode        |                                                                     |
| tex-site             | auctex                                                                    |
| sourcerer-theme      | theme                                                                    |
| powerline            |                                                                     |
