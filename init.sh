#!/bin/sh

cp .devenv-init/authorized_keys .ssh/authorized_keys
cp .devenv-init/gitconfig .gitconfig
cp .devenv-init/tmux.conf .tmux.conf
mkdir .emacs.d && cp .devenv-init/emacs.el .emacs.d/init.el

echo "Creating WORK tree"
mkdir WORK
echo "Creating PROJECTS tree"
mkdir PROJECTS

cd ~
emacs --fg-daemon --kill